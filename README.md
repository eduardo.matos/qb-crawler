# QB Crawler

Extracts all links for a given website


## Installing

```
npm install
```

## Running

```
npm run build && npm start
```

## Testing

```
npm test
```

## Environment variables

1. `QBC_SEED_URL`: Initial site URL that will be crawled.
1. `QBC_LOG_LEVEL` (`error`): One of: `silly`, `debug`, `verbose` , `info`, `warn`, `error`.
