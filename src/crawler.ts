import * as async from 'async';
import axios from 'axios';
import * as cheerio from 'cheerio';
import logger from './logger';

interface IQueueMessage {
  url: string;
}

export default class {
  public visited: string[] = [];

  private seedUrl: string;
  private queue: async.AsyncQueue<IQueueMessage>;
  private resolve: () => void;

  constructor(seedUrl: string) {
    this.seedUrl = seedUrl;

    this.queue = async.queue(this.worker.bind(this), 5);
    this.queue.drain = () => this.resolve();
  }

  public run(): Promise<undefined> {
    return new Promise(async (resolve) => {
      this.queue.push({ url: this.seedUrl });
      this.resolve = resolve;
    });
  }

  protected async worker(message: IQueueMessage, next: () => void) {
    try {
      const response = await axios.get(message.url);
      this.visited.push(message.url);
      const links = this.extractLinks(response.data);

      links.forEach((link) => {
        this.queue.push({ url: link });
      });
    } catch (err) {
      logger.error('Could not visit page');
    } finally {
      next();
    }
  }

  protected extractLinks(html: string): string[] {
    const $ = cheerio.load(html);

    const links = [];
    $('a').each((i, el) => {
      const href = $(el).attr('href');
      if (href) {
        links.push(href);
      }
    });

    return links;
  }
}
