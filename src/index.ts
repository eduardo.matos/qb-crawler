import Crawler from './crawler';

function main() {
  const crawler = new Crawler(process.env.QBC_SEED_URL);
  return crawler.run();
}

if (require.main === module) {
    (async () => await main())();
}
