import axios from 'axios';
import { expect } from 'chai';
import * as sinon from 'sinon';
import Crawler from '../src/crawler';


describe('Crawler', () => {
  let get;
  beforeEach(() => {
    get = sinon.stub(axios, 'get');
    get.resolves({ data: '' });
  });

  afterEach(() => get.restore());

  it('Visits URL', async () => {
    const crawler = new Crawler('http://site.com');

    await crawler.run();
    expect(crawler.visited).to.eql(['http://site.com']);
  });

  it('Visits returned urls', async () => {
    get.onFirstCall().resolves({
      data: '<a href="http://site.com/spam">Spam</a> <a href="http://site.com/spam2">Spam</a>',
    });
    get.resolves({ data: '' });

    const crawler = new Crawler('http://site.com');

    await crawler.run();
    expect(crawler.visited).to.eql(['http://site.com', 'http://site.com/spam', 'http://site.com/spam2']);
  });

  it('Dont save any links if page is offline', async () => {
    get.rejects();

    const crawler = new Crawler('http://site.com');

    await crawler.run();
    expect(crawler.visited).to.eql([]);
  });
});
